import liketechnik.simpleTui.annotations.ActionClass;
import liketechnik.simpleTui.annotations.ActionMethod;

@ActionClass(name="Action2", shortcut="2", description="Second action.")
public class Action2 {

    @ActionMethod
    public void execute() {
        System.out.println("Second action :)");
    }
   
}
