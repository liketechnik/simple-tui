import liketechnik.simpleTui.annotations.ActionMethod;
import liketechnik.simpleTui.annotations.ActionClass;

@ActionClass(name="Quit", shortcut="q", description="Quit the application.")
public class QuitAction {

    @ActionMethod
    public void quit() {
        System.exit(0);
    }

}
