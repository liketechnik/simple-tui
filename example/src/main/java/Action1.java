import liketechnik.simpleTui.annotations.ActionClass;
import liketechnik.simpleTui.annotations.ActionMethod;

@ActionClass(name="Action 1", shortcut="1", description="First action.")
public class Action1 {

    @ActionMethod(withData = true)
    public void doSmth(Data data) {
        data.actionExecutions++;
        System.out.println("Executed " + data.actionExecutions + " times!");
    }

}
