# simple_tui

Small annotation-based java lib for terminal-ui applications in java.
Used for small project at university.

## Usage

### With gradle
- Add as dependency in gradle.
- Change mainClass to "liketechnik.simpleTui.runtime.Main".
- pass the fully qualified name of your ApplicationClass (see below) as the first argument to the java program

### Source code

- annotate one class with the ApplicationClass annotation
- annotate one or more classes with actionclass and give each of these exactly one method annotated with ActionMethod

### Additional tips

Look at the annotations in the `annotations` package. 
These contain additional documentation, also for features not documented 
here (yet).
