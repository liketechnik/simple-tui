package liketechnik.simpleTui.io;

import java.util.Scanner;
import java.util.function.Predicate;

/**
 * Provides a method for getting and validating user input.
 *
 * @author Jonas Patrick Voellmecke, Florian Warzecha
 * @version 10-Jun-2020
 */
public class ConsoleIn {

    /**
     * Gets input from the user and validates it with all of the passed validators.
     *
     * <p>If validation fails asks the user to enter valid input.
     */
    @SafeVarargs
    public static String userInput(Predicate<String>... validators) {
        Scanner in = new Scanner(System.in);
        String input;

        boolean validated = true;
        do {
            input = in.nextLine();

            validated = true;
            for (Predicate<String> validator : validators) {
                if (!validator.test(input)) {
                    validated = false;
                    System.out.println("Please enter a valid input!");
                    break;
                }
            }
        } while (!validated);

        return input;
    }

    /**
     * Provides a default implementation to validate that an input is a number.
     */
    public static class IntegerPredicate implements Predicate<String> {

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean test(String s) {
            try {
                Integer.parseInt(s);
            } catch (NumberFormatException e) {
                return false;
            }
            return true;
        }
    }
}
