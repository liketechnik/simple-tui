package liketechnik.simpleTui.io;

import java.util.HashMap;
import java.util.function.Predicate;

/**
 * Validate a selection from the key-set of a {@link HashMap}.
 *
 * @author Florian Warzecha
 * @version 14-Jun-2020
 */
public class HashMapSelectionPredicate implements Predicate<String> {

    /** The {@link HashMap} to validate from. */
    private HashMap<String, ?> map;

    /**
     * Create a new instance for validating selection from the
     * given {@code map}'s keys.
     *
     * <p>The {@code map}'s keys must be uppercase.
     *
     * @param map The HashMap to use for validation.
     */
    public HashMapSelectionPredicate(HashMap<String, ?> map) {
        this.map = map;
    }

    /**
     * Checks if the {@code map} contains the given string in its keyset.
     *
     * <p>Checks against the upper-case version of {@code s}, so
     * all checks are case-insensitive.
     *
     * @param s Test if {@code s} is contained in {@link #map}'s keyset.
     * @return {@code s} is a valid selection for {@link #map}.
     */
    @Override
    public boolean test(String s) {
        return map.containsKey(s.toUpperCase());
    }
}
