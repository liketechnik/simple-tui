package liketechnik.simpleTui.runtime;

import java.lang.reflect.InvocationTargetException;

import liketechnik.simpleTui.annotations.ApplicationClass;

public class Main {

    public static void main(String[] args) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
        Class<?> application = null;
        try {
            application = Class.forName(args[0]);
        } catch (ClassNotFoundException e) {
            System.err.println("Specified application (group) class not found!");
            System.exit(-1);
        }

        if (application.getAnnotation(ApplicationClass.class) != null) {
            Application app = new Application(application);
            while (true) {
                app.executeOnce();
            }
        } else {
            ApplicationGroup appGroup = new ApplicationGroup(application);
            appGroup.execute();
        }
    }
}
