package liketechnik.simpleTui.runtime;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import liketechnik.simpleTui.annotations.ApplicationClass;
import liketechnik.simpleTui.io.ConsoleIn;
import liketechnik.simpleTui.io.HashMapSelectionPredicate;

/**
 * Hold an {@link ApplicationClass} at runtime.
 *
 * @author Florian Warzecha
 * @version 10-Jun-2020
 */
class Application {

    Class<?> clazz;
    ApplicationClass clazzInfo;

    Object data;

    HashMap<String, Action> actions;

    /**
     * Read the metadata from a given action class and construct a new usable instance.
     */
    public Application(Class<?> app) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
        actions = new HashMap<>();

        clazz = app;
        clazzInfo = app.getAnnotation(ApplicationClass.class);

        if (clazzInfo == null) {
            System.err.println("Application " + clazz.getName() + " is not annotated!");
            System.exit(-3);
        }

        for (Class<?> action : clazzInfo.actions()) {
            Action act = new Action(action);
            actions.put(act.getShortcut(), act);
        }

        data = clazzInfo.data().getConstructor().newInstance();
    }

    /** Show the menu and execute the selected action. */
    public void executeOnce() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        for (Action action : actions.values()) {
            StringBuilder actionItem = new StringBuilder("[");
            actionItem.append(action.getShortcut());
            actionItem.append("] ");
            actionItem.append(action.getName());
            actionItem.append(": ");
            actionItem.append(action.getDescription());
            System.out.println(actionItem);
        }

        System.out.println();

        System.out.println("Please enter selection: ");

        String selection = ConsoleIn.userInput(new HashMapSelectionPredicate(actions)).toUpperCase();

        System.out.println();
        actions.get(selection).action(data);
        System.out.println();
    }

}
