package liketechnik.simpleTui.runtime;

import java.lang.reflect.InvocationTargetException;

import liketechnik.simpleTui.annotations.ActionClass;

/**
 * Holds an {@link ApplicationClass} at runtime.
 *
 * <p>Used for {@link ApplicationClass}es that belong to a
 * {@link ApplicationGroupClass}. Those are additionally
 * annotated with {@link ActionClass} whose metadata is provided
 * too with this class.
 */
class GroupedApplication extends Application {

    ActionClass clazzInfo;

    /**
     * Read the metadata from the given grouped application class
     * and provide it at runtime.
     */
    public GroupedApplication(Class<?> app) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
        super(app);

        clazzInfo = app.getAnnotation(ActionClass.class);

        if (clazzInfo == null) {
            System.err.println("Grouped application " + clazz.getName() + " is not annotated!");
            System.exit(-3);
        }
    }

    /**
     * The name of the application.
     */
    public String getName() {
        return clazzInfo.name();
    }

    /**
     * The shortcut to select the application.
     */
    public String getShortcut() {
        return clazzInfo.shortcut();
    }

    /**
     * The description of the application.
     */
    public String getDescription() {
        return clazzInfo.description();
    }
}
