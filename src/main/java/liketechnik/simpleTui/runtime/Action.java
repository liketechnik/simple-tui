package liketechnik.simpleTui.runtime;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import liketechnik.simpleTui.annotations.ActionClass;
import liketechnik.simpleTui.annotations.ActionMethod;
import liketechnik.simpleTui.annotations.ExceptionHandler;

/**
 * Hold an action at runtime.
 *
 * @author Florian Warzecha
 * @version 10-Jun-2020
 */
class Action {

    Class<?> clazz;
    ActionClass clazzInfo;
    ActionMethod methodInfo;
    Method action;
    List<ExceptionPair> exceptionHandler;
    Object instance;

    private class ExceptionPair {
        public Method handler;
        public ExceptionHandler info;

        public ExceptionPair(Method handler, ExceptionHandler info) {
            this.handler = handler;
            this.info = info;
        }
    }

    /**
     * Read the metadata from a given action class and construct a new usable instance.
     */
    public Action(Class<?> action) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
        clazz = action;
        clazzInfo = clazz.getAnnotation(ActionClass.class);

        if (clazzInfo == null) {
            System.err.println("Action " + clazz.getName() + " is not annotated!");
            System.exit(-3);
        }

        exceptionHandler = new ArrayList<>();
        for (Method method : clazz.getMethods()) {
            // TODO cancel if multiple annotated methods
            ActionMethod methodInfo = method.getAnnotation(ActionMethod.class);
            if (methodInfo != null) {
                this.methodInfo = methodInfo;
                this.action = method;
            }
            ExceptionHandler exceptionInfo = method.getAnnotation(ExceptionHandler.class);
            if (exceptionInfo != null) {
                exceptionHandler.add(new ExceptionPair(method, exceptionInfo));
            }
        }

        if (methodInfo == null) {
            System.err.println("No annotated method for action " + action.getName());
            System.exit(-3);
        }

        instance = clazz.getConstructor().newInstance();
    }

    /** Return the name of this action. */
    public String getName() {
        return clazzInfo.name();
    }

    /**
     * Return the uppercase version of the shortcut.
     */
    public String getShortcut() {
        return clazzInfo.shortcut().toUpperCase();
    }

    /** Return the description of this action.  */
    public String getDescription() {
        return clazzInfo.description();
    }

    /**
     * Execute the method for this action.
     *
     * <p>Ignores data if the {@link ActionMethod} has not specified {@code withData} true.
     *
     * @param data The data object of the {@link Application}.
     */
    public void action(Object data) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        try {
            if (methodInfo.withData()) {
                action.invoke(instance, data);
            } else {
                action.invoke(instance);
            }
        } catch (InvocationTargetException e) {
            Method handler = null;
            for (ExceptionPair pair : exceptionHandler) {
                if (pair.info.type().isAssignableFrom(e.getCause().getClass())) {
                    handler = pair.handler;
                }
                if (handler != null) {
                    break;
                }
            }

            if (handler == null) {
                throw e;
            } else {
                handler.invoke(instance, e.getCause());
            }
        }
    }

}
