package liketechnik.simpleTui.runtime;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import liketechnik.simpleTui.annotations.ApplicationGroupClass;
import liketechnik.simpleTui.io.ConsoleIn;
import liketechnik.simpleTui.io.HashMapSelectionPredicate;

/**
 * Hold an {@link ApplicationGroupClass} at runtime.
 *
 * @author Florian Warzecha
 * @version 14-Jun-2020
 */
class ApplicationGroup {

    Class<?> clazz;
    ApplicationGroupClass clazzInfo;

    HashMap<String, GroupedApplication> applications;

    /**
     * Read the metadata from a given action class and construct a new usable instance.
     */
    public ApplicationGroup(Class<?> appGroup) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
       applications = new HashMap<>();

       clazz = appGroup;
       clazzInfo = appGroup.getAnnotation(ApplicationGroupClass.class);

       if (clazzInfo == null) {
           System.err.println("Application " + clazz.getName() + " is not annotated!");
           System.exit(-3);
       }

       for (Class<?> application : clazzInfo.applications()) {
           GroupedApplication app = new GroupedApplication(application);
           applications.put(app.getShortcut(), app);
       }
    }

    /**
     * Show the menu and execute the select application.
     */
    public void execute() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        for (GroupedApplication app : applications.values()) {
            StringBuilder appItem = new StringBuilder("[");
            appItem.append(app.getShortcut());
            appItem.append("] ");
            appItem.append(app.getName());
            appItem.append(": ");
            appItem.append(app.getDescription());
            System.out.println(appItem);
        }

        System.out.println("Please enter selection: ");

        String selection = ConsoleIn.userInput(new HashMapSelectionPredicate(applications)).toUpperCase();

        System.out.println();
        while (true) {
            applications.get(selection).executeOnce();
        }
    }

}
