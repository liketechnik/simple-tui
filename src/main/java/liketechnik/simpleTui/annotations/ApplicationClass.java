package liketechnik.simpleTui.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a class as an terminal-ui application.
 *
 * <p>The class can be empty and is purely used for metadata via this annotation.
 *
 * <p>Internally loads the actions from the specified classes, instantiates the given
 * data class and repeatedly shows the user a menu consisting of all defined actions.
 *
 * <p>If the same class is annotated with {@link ActionClass} it can be
 * used for an {@link ApplicationGroupClass}.
 *
 * @author Florian Warzecha
 * @version 10-Jun-2020
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ApplicationClass {

    /** The possible actions a user can choose. */
    Class<?>[] actions();

    /**
     * The class holding data for the application.
     *
     * <p>The class MUST have a constructor without parameters.
     */
    Class<?> data() default Object.class;

}
