package liketechnik.simpleTui.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;

/**
 * Declare an {@link ActionClass} method to
 * as an error handler for that actions {@link ActionMethod}.
 *
 * <p>It is expected that the method accepts exactly one parameter
 * of type {@link Throwable}. (Or a subtype if {@link code} is specified.)
 *
 * @author Florian Warzecha
 * @version 10-Jun-2020
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ExceptionHandler {

    Class<? extends Throwable> type() default Throwable.class;

}
