package liketechnik.simpleTui.annotations;

import java.lang.annotation.*;

/**
 * Marks a method to be executed for as the action of a {@link ActionClass}.
 *
 * <p>Each {@link ActionClass} can only have exactly one method annotated with this.
 *
 * <p>If {@link #withData} is {@code true}, the method is expected to accept exactly
 * one parameter with the type of {@link ApplicationClass#data}. Else it needs to be
 * parameterless.
 *
 * @author Florian Warzecha
 * @version 10-Jun-2020
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ActionMethod {

    /** If the method has a parameter accepts a {@link ApplicationClass#data} object.  */
    boolean withData() default false;

}
