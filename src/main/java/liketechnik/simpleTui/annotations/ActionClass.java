package liketechnik.simpleTui.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;

/**
 * Marks a class as a possible user action.
 *
 * <p>Each class can only provide exactly one action method,
 * e. g. each class represents exactly one possible action.
 *
 * <p>The data of this annotation is used to present a menu to
 * the user.
 *
 * <p>Also used to mark an {@link ApplicationClass} as part of
 * {@link ApplicationGroupClass}.
 *
 * @author Florian Warzecha
 * @version 10-Jun-2020
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ActionClass {

    /** The name of this action. */
    String name();
    /** The shortcut (user input) to select this action. */
    String shortcut();
    /** The description of this action. */
    String description();

}
