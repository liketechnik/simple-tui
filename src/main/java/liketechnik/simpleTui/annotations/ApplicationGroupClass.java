package liketechnik.simpleTui.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.Retention;

/**
 * Marks a class as a group of terminal-ui (sub-)applications.
 *
 * <p>The class can be empty and is purely used for metadata via this annotation.
 *
 * <p>Internally loads the applications from the specified classes,
 * presents their names to the user and starts the selected one.
 *
 * <p>Note that the application classes MUST be annotated with both
 * {@link ApplicationClass} AND {@link ActionClass}.
 *
 * @author Florian Warzecha
 * @version 14-Jun-2020
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ApplicationGroupClass {

    Class<?>[] applications();

}
